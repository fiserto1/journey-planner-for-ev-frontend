/*
To enable middle points
    1) set MIDDLE_POINT_LIMIT
    2) uncomment "$("#add-point-icon").click(onAddPointClick);"
    3) remove class "disabled-icon" for element "#add-point-icon" in index.html
 */

var MIDDLE_POINT_LIMIT = 0;

var START_MARKER_ICON = L.AwesomeMarkers.icon({
    prefix: 'fa', //font awesome
    markerColor: 'blue',
    icon: 'font'
});
var DESTINATION_MARKER_ICON = L.AwesomeMarkers.icon({
    prefix: 'fa', //font awesome
    markerColor: 'blue',
    icon: 'bold'
});
var SUPERCHARGER_MARKER_ICON = L.AwesomeMarkers.icon({
    prefix: 'fa', //font awesome
    markerColor: 'green',
    icon: 'plug'
});
var CHEAPEST_SUPERCHARGER_MARKER_ICON = L.AwesomeMarkers.icon({
    prefix: 'fa', //font awesome
    markerColor: 'green',
    icon: 'plug'
});
var FASTEST_SUPERCHARGER_MARKER_ICON = L.AwesomeMarkers.icon({
    prefix: 'fa', //font awesome
    markerColor: 'red',
    icon: 'plug'
});

var MO_FASTEST_SUPERCHARGER_MARKER_ICON = L.AwesomeMarkers.icon({
    prefix: 'fa', //font awesome
    markerColor: 'blue',
    icon: 'plug'
});

var allMarkers = [];
var dragIndex;
var superchargerMarkers = [];

/**
 * create start and destination markers,
 * set sortable to search group,
 * set action listenners for markers
 */
function initializeMarkers() {
    var startMarker = L.marker(null, {
        icon: START_MARKER_ICON,
        draggable: true
    });

    var destinationMarker = L.marker(null, {
        icon: DESTINATION_MARKER_ICON,
        draggable: true
    });
    //superchargerMarkers = L.layerGroup();
    allMarkers.push(startMarker);
    allMarkers.push(destinationMarker);

    $("#change-direction-icon").click(onChangeDirectionClick);
    $(".remove-point").click(onRemovePointClick);

    startMarker.on('dragend', onMarkerDrag);
    destinationMarker.on('dragend', onMarkerDrag);

    setShowRemoveOnFocus($("input"));

    //parse inserted coordinates
    $(".form-control").keyup(function (e) {
        //ENTER PRESSED IN INPUT -> COORDINATE INSERTED MANUALLY
        var key = e.which;
        if(key == 13) {
            var inputIndex = $(e.target).parent().index();
            var vals = e.target.value.split(",");

            if (vals.length == 2 && $.isNumeric(vals[0]) && $.isNumeric(vals[1])) {
                var latLng = L.latLng(vals[0], vals[1]);
                allMarkers[inputIndex].setLatLng(latLng).addTo(map);
                map.setView(latLng);
                getPlansFromServer();
            } else {
                //$("#error-panel").text("Wrong input!").show();
            }
            closeAC();
        }
    });

    $("#search-group").sortable({
        handle: ".drag-drop",
        update: function (event, ui) {
            refreshSearchGroup();
            var dropIndex = ui.item.index();
            var markerToMove = allMarkers[dragIndex];
            allMarkers.splice(dragIndex, 1);
            allMarkers.splice(dropIndex, 0, markerToMove);
            allMarkers[0].setIcon(START_MARKER_ICON);
            allMarkers[allMarkers.length-1].setIcon(DESTINATION_MARKER_ICON);
            for(var i = 1; i<allMarkers.length-1; i++) {
                allMarkers[i].setIcon(SUPERCHARGER_MARKER_ICON);
            }
            getPlansFromServer();
        },
        start: function (event, ui) {
            dragIndex = ui.item.index();
        }
    });
}

/**
 * action listener for filling search input
 * @param e event
 * @param input active input element
 */
function onMapClick(e, input) {
    var latLon = e.latlng;

    var $input = $(input.target);
    var inputIndex = $input.parent().index();

    findAddressFromCoords(inputIndex, latLon);

    allMarkers[inputIndex].setLatLng(latLon).addTo(map);
    map.off("click");
    getPlansFromServer();
}

/**
 * fill search input on marker drag
 * @param e event
 */
function onMarkerDrag(e) {
    var markerIndex = allMarkers.indexOf(e.target);
    findAddressFromCoords(markerIndex, e.target.getLatLng());
    getPlansFromServer();
}


function addNewStartPoint() {
    var searchGroup = $("#search-group");
    if (searchGroup.children().length < (MIDDLE_POINT_LIMIT + 2)) {

        searchGroup.find(".start-icon").addClass("middle-point-icon");
        searchGroup.find(".start-icon").removeClass("start-icon");
        var startInput = $(".search-start");
        startInput.addClass("search-middle-point");
        startInput.removeClass("search-start");

        var inputGroup = $("<div>").addClass("input-group");
        var markerAddon = $("<div>").addClass("input-group-addon drag-drop");
        var closeAddon = $("<div>").addClass("input-group-addon right-addon");
        var closeButton = $("<button type='button' tabindex='-1'>").addClass("close remove-point");
        closeButton.click(onRemovePointClick);
        $("<span>").html("&times").appendTo(closeButton);
        closeButton.appendTo(closeAddon);
        $("<i>").addClass("fa fa-map-marker start-icon").appendTo(markerAddon);
        markerAddon.appendTo(inputGroup);
        var searchInput = $("<input type='search'>").addClass("form-control search-start whisper");
        setShowRemoveOnFocus(searchInput);
        searchInput.appendTo(inputGroup);
        closeAddon.appendTo(inputGroup);
        inputGroup.prependTo(searchGroup);

        setAC();

        if (searchGroup.children().length == (MIDDLE_POINT_LIMIT + 2)) {
            $("#add-point-icon").addClass("disabled-icon");
        }

        allMarkers[0].setIcon(SUPERCHARGER_MARKER_ICON);

        var newMarker = L.marker(null, {
            icon: START_MARKER_ICON,
            draggable: true
        });
        newMarker.on('dragend', onMarkerDrag);
        allMarkers.unshift(newMarker);
    }
}

function addNewDestinationPoint() {
    var searchGroup = $("#search-group");
    if (searchGroup.children().length < (MIDDLE_POINT_LIMIT + 2)) {

        searchGroup.find(".destination-icon").addClass("middle-point-icon");
        searchGroup.find(".destination-icon").removeClass("destination-icon");
        var destinationInput = $(".search-destination");
        destinationInput.addClass("search-middle-point");
        destinationInput.removeClass("search-destination");

        var inputGroup = $("<div>").addClass("input-group");
        var markerAddon = $("<div>").addClass("input-group-addon drag-drop");
        var closeAddon = $("<div>").addClass("input-group-addon right-addon");
        var closeButton = $("<button type='button'>").addClass("close remove-point");
        closeButton.click(onRemovePointClick);
        $("<span>").html("&times").appendTo(closeButton);
        closeButton.appendTo(closeAddon);
        $("<i>").addClass("fa fa-map-marker destination-icon").appendTo(markerAddon);
        markerAddon.appendTo(inputGroup);
        var searchInput = $("<input type='search'>").addClass("form-control search-destination whisper");
        setShowRemoveOnFocus(searchInput);
        searchInput.appendTo(inputGroup);
        closeAddon.appendTo(inputGroup);
        inputGroup.appendTo(searchGroup);

        setAC();

        if (searchGroup.children().length == (MIDDLE_POINT_LIMIT + 2)) {
            $("#add-point-icon").addClass("disabled-icon");
        }

        allMarkers[allMarkers.length-1].setIcon(SUPERCHARGER_MARKER_ICON);
        var newMarker = L.marker(null, {
            icon: DESTINATION_MARKER_ICON,
            draggable: true
        });
        newMarker.on('dragend', onMarkerDrag);
        allMarkers.push(newMarker);
        searchInput.focus();
    }
}

/**
 * action listener for remove cross in input
 */
function onRemovePointClick() {
    var wholeInput = $(this).parent().parent();
    map.removeLayer(allMarkers[wholeInput.index()]);
    allMarkers[wholeInput.index()].setLatLng(null);
    $(this).parent().prev().val("");
    getPlansFromServer();
}



/**
 * refresh search group after search group change
 */
function refreshSearchGroup() {
    var searchGroup = $("#search-group");
    $(".form-control").removeClass("search-start search-destination");
    searchGroup.find(".fa-map-marker").removeClass("start-icon destination-icon");
    var allForms = searchGroup.children();
    allForms.eq(0).find("i").addClass("start-icon");
    allForms.eq(0).find("input").addClass("search-start");
    allForms.eq(1).find("i").addClass("destination-icon");
    allForms.eq(1).find("input").addClass("search-destination");
}

/**
 * set show remove cross in input on focus
 * @param focusedElement focused input
 */
function setShowRemoveOnFocus(focusedElement) {
    focusedElement.focus(function (eFocus) {
        //todo focus-in doesnt work
        $(this).next().children().addClass("focus-in");
        map.off("click");
        map.on("click", function(eClick) {
            onMapClick(eClick,eFocus);
        });
    });
    focusedElement.blur(function (e) {
        var mapDiv = document.getElementById("map");
        if (e.relatedTarget != mapDiv) {
            map.off("click");
        }
        $(this).next().children().removeClass("focus-in");
    });
}


function addPlanSuperchargerMarkers(chargers, planId) {
    superchargerMarkers[planId] = L.layerGroup();
    //for (var i = 0; i < chargers.length; i++) {
    //    console.log(chargers[i].type);
    //    console.log(chargers[i].pricePerWh);
    //}
    //skip origin and destination // first is origin and last is destination
    for (var i = 0; i < chargers.length; i++) {
        var superchargerMarker = L.marker(null, {
            icon: SUPERCHARGER_MARKER_ICON,
            draggable: false
        });
        var journeyType = response.plans[planId].type;
        if (journeyType == CHEAPEST_JOURNEY) {
            superchargerMarker.setIcon(CHEAPEST_SUPERCHARGER_MARKER_ICON)
        } else if (journeyType == FASTEST_JOURNEY) {
            superchargerMarker.setIcon(FASTEST_SUPERCHARGER_MARKER_ICON);
        } else if (journeyType == MO_FASTEST_JOURNEY) {
            superchargerMarker.setIcon(MO_FASTEST_SUPERCHARGER_MARKER_ICON);
        } else {
            error.log("Journey type not recognized.")
        }
        var latLon = createLatLng(chargers[i].coordinates);
        superchargerMarker.setLatLng(latLon);
        var maxSoc = 85000;
        superchargerMarker.bindPopup("<dl><dt>Supercharger</dt>" +
            "<dd>Price: " + chargers[i].pricePerWh + " cents per kWh</dd>"
            + "<dd>Charge To: "+ ((chargers[i].leavingStateOfCharge/maxSoc)*100).toFixed(0) + " \%</dd>"
            + "<dd>Charging Time: "+ (chargers[i].chargingTime/60).toFixed(0) + " min</dd></dl>");
        superchargerMarkers[planId].addLayer(superchargerMarker);
    }
    //superchargerMarkers[planId].addTo(map);
}

/**
 * action listener for change diraction
 */
function onChangeDirectionClick() {
    allMarkers.reverse();
    allMarkers[0].setIcon(START_MARKER_ICON);
    var destinationIndex = allMarkers.length-1;
    allMarkers[destinationIndex].setIcon(DESTINATION_MARKER_ICON);
    for(var i = 1; i < destinationIndex; i++) {
        allMarkers[i].setIcon(SUPERCHARGER_MARKER_ICON);
    }
    reverseSearchForm();
    getPlansFromServer();
}

function reverseSearchForm() {
    var fromTopIndex = 0;
    var fromBottomIndex = allMarkers.length-1;
    var limit = Math.floor(allMarkers.length/2);
    var allForms = $("#search-group").children();
    while (limit > fromTopIndex) {
        var fromTopInput = allForms.eq(fromTopIndex).find("input");
        var fromBottomInput = allForms.eq(fromBottomIndex).find("input");
        var value = fromBottomInput.val();
        fromBottomInput.val(fromTopInput.val());
        fromTopInput.val(value);
        fromTopIndex++;
        fromBottomIndex--;
    }
}