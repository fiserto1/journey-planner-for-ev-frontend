
/**
 * create one route button element and add to routes panel
 * @param plan
 * @param routeIndex
 */
function createButtonForRoute(plan, routeIndex) {

    var routeButton = $("<div>").addClass("route-but col-md-12");
    //id je index daneho planu v polich polyline, nezamenit se zobrazovanym poradim na strance
    routeButton.attr("id", "route-but-" + routeIndex);
    var routeDiv = $("<div>").addClass("route-desc row");
    var colorTab = createColorTab(plan, routeIndex);
    //var colorTab2 = createColorTab(plan, routeIndex);
    var travelTimeTab = createTravelTimeTab(plan, routeIndex);
    var lengthTab = createLengthTab(plan, routeIndex);
    var consumptionTab = createConsumptionTab(plan, routeIndex);
    var priceTab = createPriceTab(plan, routeIndex);
    var socChartTab = createSocChartTab(plan, routeIndex);
    //var planViewTab = createPlanViewTab(plan, routeIndex);
    colorTab.appendTo(routeDiv);
    travelTimeTab.appendTo(routeDiv);
    lengthTab.appendTo(routeDiv);
    //colorTab2.appendTo(routeDiv);
    consumptionTab.appendTo(routeDiv);
    priceTab.appendTo(routeDiv);
    socChartTab.appendTo(routeDiv);
    //planViewTab.appendTo(routeDiv);

    routeDiv.appendTo(routeButton);
    routeButton.appendTo($("#routes-panel"));
}

function createColorTab(plan, routeIndex) {

    var colorTab = $("<div>").addClass("color-tab");
    if (plan.type == FASTEST_JOURNEY) {
        colorTab.addClass("fast-color");
    } else if (plan.type == CHEAPEST_JOURNEY) {
        colorTab.addClass("eco-color");
    } else {
        colorTab.addClass("mo-fast-color");
    }
    //var colorTab = $("<div>").addClass("color-tab col-md-1");
    //var travelTimeIconBut = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("traveltime-icon");
    //travelTimeIconBut.attr("id", "traveltime-button");
    //var travelTimeIcon = $("<i>").addClass("fa fa-clock-o fa-1x");
    //travelTimeIcon.appendTo(travelTimeIconBut);
    //travelTimeIconBut.appendTo(colorTab);
    return colorTab;
}

function createTravelTimeTab(plan, routeIndex) {

    var planTravelTimeInMins = (plan.travelTime/60).toFixed(0);
    var travelTimeTab = $("<div>").addClass("description-tab main-criteria col-md-6");

    var travelTimeIconBut = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("traveltime-icon");
    travelTimeIconBut.attr("id", "traveltime-button");
    var travelTimeIcon = $("<i>").addClass("fa fa-clock-o fa-1x");
    travelTimeIcon.appendTo(travelTimeIconBut);
    travelTimeIconBut.appendTo(travelTimeTab);

    var travelTimeLabel = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("one-description");
    var travelTimeValue = $('<span>').addClass("description-value");
    if (planTravelTimeInMins >= 60) {
        travelTimeValue.text(Math.floor(planTravelTimeInMins/60) + " h " + planTravelTimeInMins%60 + " min");
    } else {
        travelTimeValue.text(planTravelTimeInMins + " min");
    }
    travelTimeValue.appendTo(travelTimeLabel);
    travelTimeLabel.appendTo(travelTimeTab);

    return travelTimeTab;
}

function createLengthTab(plan, routeIndex) {

    var length = plan.length;
    var lengthTab = $("<div>").addClass("description-tab main-criteria col-md-6");

    var lengthIconBut = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("length-icon");
    lengthIconBut.attr("id", "length-button");
    var lengthIcon = $("<i>").addClass("fa fa-road fa-1x");
    //var lengthIcon2 = $("<img src='distance.png'>").addClass("dis-icon");
    lengthIcon.appendTo(lengthIconBut);
    lengthIconBut.appendTo(lengthTab);

    var lengthDesc = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("one-description");
    var lengthLabel = $('<span>').addClass("description-label translate");
    lengthLabel.appendTo(lengthDesc);
    var lengthValue = $('<span>').addClass("description-value");
    if (length < 1000) {
        lengthValue.text(length + " m");
    } else {
        lengthValue.text((length/1000).toFixed(1) + " km");
    }
    lengthValue.appendTo(lengthDesc);
    lengthDesc.appendTo(lengthTab);
    return lengthTab;
}

function createConsumptionTab(plan, routeIndex) {

    var consumption = plan.consumption;
    var consumptionTab = $("<div>").addClass("description-tab main-criteria col-md-6");

    var consIconBut = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("consumption-icon");
    consIconBut.attr("id", "consumption-button");
    var consumptionIcon = $("<i>").addClass("fa fa-bolt fa-1x");
    //var lengthIcon2 = $("<img src='distance.png'>").addClass("dis-icon");
    consumptionIcon.appendTo(consIconBut);
    consIconBut.appendTo(consumptionTab);

    var consumptionDesc = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("one-description");
    var consumptionLabel = $('<span>').addClass("description-label translate");
    consumptionLabel.appendTo(consumptionDesc);
    var consumptionValue = $('<span>').addClass("description-value");
    consumptionValue.text((consumption/1000).toFixed(1) + " kWh");
    //if (consumption < 1000) {
    //    consumptionValue.text(consumption + " Wh");
    //} else {
    //    consumptionValue.text((consumption/1000).toFixed(1) + " kWh");
    //}
    consumptionValue.appendTo(consumptionDesc);
    consumptionDesc.appendTo(consumptionTab);
    return consumptionTab;
}

function createPriceTab(plan, routeIndex) {

    var price = plan.price;
    var priceTab = $("<div>").addClass("description-tab main-criteria col-md-6");

    var priceIconBut = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("price-icon");
    priceIconBut.attr("id", "price-button");
    var priceIcon = $("<i>").addClass("fa fa-money fa-1x");
    //var lengthIcon2 = $("<img src='distance.png'>").addClass("dis-icon");
    priceIcon.appendTo(priceIconBut);
    priceIconBut.appendTo(priceTab);

    var priceDesc = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("one-description");
    var priceLabel = $('<span>').addClass("description-label translate");
    priceLabel.appendTo(priceDesc);
    var priceValue = $('<span>').addClass("description-value");
    priceValue.text((price/100).toFixed(2) + " Eur");
    priceValue.appendTo(priceDesc);
    priceDesc.appendTo(priceTab);
    return priceTab;
}

function createSocChartTab(plan, routeIndex) {
    var socTab = $("<div>").addClass("description-tab plan-view col-md-12");
    var divId = "soc-chart-" + routeIndex;
    var socChartDiv = $("<div>").attr("id", divId).addClass("small-chart row");
    socChartDiv.appendTo(socTab);
    return socTab;
}

function createPlanViewTab(plan, routeIndex) {
    var planViewTab = $("<div>").addClass("description-tab plan-view col-md-12");
    var startMarkerBut = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-icon");
    var startMarkerIcon = $("<i>").addClass("fa fa-map-marker start-icon");
    startMarkerIcon.appendTo(startMarkerBut);
    startMarkerBut.appendTo(planViewTab);

    var routeLineSpan = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-span");
    var segmentLine = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-line col-md-12");
    var segmentLengthDiv = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-length col-md-12");
    var segmentLengthValue = plan.stopovers[0].distanceToNextStopover;
    if (segmentLengthValue < 1000) {
        segmentLengthDiv.text(segmentLengthValue + " m");
    } else {
        segmentLengthDiv.text((segmentLengthValue/1000).toFixed(1) + " km");
    }
    segmentLine.appendTo(routeLineSpan);
    segmentLengthDiv.appendTo(routeLineSpan);
    routeLineSpan.appendTo(planViewTab);


    for (var i = 0; i < plan.stopovers.length - 2; i++) {
        var superchargerMarkerBut = $('<span data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-icon plan-view-icon-plug");
        superchargerMarkerBut.attr('data-original-title', 'Charging time: ' + (plan.stopovers[i+1].chargingTime/60).toFixed(0) + ' mins')
        var superchargerMarkerIcon = $("<i>").addClass("fa fa-plug");
        superchargerMarkerIcon.appendTo(superchargerMarkerBut);
        superchargerMarkerBut.appendTo(planViewTab);

        routeLineSpan = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-span");
        segmentLine = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-line col-md-12");
        segmentLengthDiv = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-length col-md-12");
        segmentLengthValue = plan.stopovers[i+1].distanceToNextStopover;
        if (segmentLengthValue < 1000) {
            segmentLengthDiv.text(segmentLengthValue + " m");
        } else {
            segmentLengthDiv.text((segmentLengthValue/1000).toFixed(1) + " km");
        }
        segmentLine.appendTo(routeLineSpan);
        segmentLengthDiv.appendTo(routeLineSpan);
        routeLineSpan.appendTo(planViewTab);
    }

    var destMarkerBut = $('<div data-toggle="tooltip" data-placement="bottom">').addClass("plan-view-icon");
    var destMarkerIcon = $("<i>").addClass("fa fa-map-marker destination-icon");
    destMarkerIcon.appendTo(destMarkerBut);
    destMarkerBut.appendTo(planViewTab);

    return planViewTab;
}

function updateSegmentLengthRatio(plan, index) {
    var sumOfDistances = 0;
    var sumOfIconOuterWidths = 0;
    var icons = $('.plan-view-icon');
    for (var i = 0; i < icons.length; i++) {
        sumOfIconOuterWidths += icons.eq(i).outerWidth(true);
    }

    //skip destination stopover
    for (var i = 0; i < plan.stopovers.length-1; i++) {
        sumOfDistances += plan.stopovers[i].distanceToNextStopover;
    }

    var segmentLines = $('.plan-view-span');
    for (var i = 0; i < plan.stopovers.length-1; i++) {
        var percSegmentDistance = (plan.stopovers[i].distanceToNextStopover/sumOfDistances)*100;
        var parentWidth = $('.plan-view').width();
        percSegmentDistance = percSegmentDistance * (parentWidth-sumOfIconOuterWidths)/parentWidth;

        segmentLines.eq(i).css('width', percSegmentDistance+'%');
        if (percSegmentDistance < 15) {
            var segmentLengthDiv = $('.plan-view-length').eq(i);
            segmentLines.eq(i).attr('data-original-title', segmentLengthDiv.text());
            segmentLengthDiv.hide();
            $('[data-toggle="tooltip"]').tooltip();

        }
    }
}