
var map;
var lastClickedPosition = null;

/**
 * create map with mapbox tiles
 * set context menu, layers, locate, legend, autocomplete
 */
function initializeMap() {
    var contextMenuItems = createContextMenuItems();

    //var CartoDB_DarkMatter = L.tileLayer('http://{s}.basemaps.cartocdn.com/dark_all/{z}/{x}/{y}.png', {
    //    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
    //    subdomains: 'abcd',
    //    maxZoom: 19
    //});

    //v8

    //var streetsMap = L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    var streetsMap = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/streets-v10/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
        maxZoom: 18,
        id: 'fiserto1.3ed94b1c',
        accessToken: 'pk.eyJ1IjoiZmlzZXJ0bzEiLCJhIjoiNmE1NzkzMjQ5ZjdhYTMxZDllNzhlNmQxNGMzZGIyMTAifQ.2xblvAvcBqHdhd3GnKNrbQ',
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, ' +
            '<a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery &copy; ' +
            '<a href="http://mapbox.com">Mapbox</a>'
    });

    map = L.map('map', {
        layers: streetsMap,
        zoomControl: false,
        contextmenu: true,
        contextmenuItems: contextMenuItems
    }).setView([51.5160, 10.3807], 7);

    map.on("contextmenu", function (e) {
        lastClickedPosition = e.latlng;
    });

    L.control.zoom({position: "topright"}).addTo(map);
    L.control.locate({position: 'topright', keepCurrentZoomLevel: false, locateOptions:{maxZoom: 15}}).addTo(map);

    //asynchronous
    addSuperchargersLayerFromFile();

    var legend = L.control({position: 'bottomright'});
    legend.onAdd = function () {
        return document.getElementById("legend");
    };
    legend.addTo(map);
    $("#legend").hide();
    
    //var featureLayer = L.mapbox.featureLayer()
    //    .loadURL('./json/GE-to-trunk-multi22.geojson')
    //    .addTo(map);
}

function createContextMenuItems() {
    return [{
        text: "Add start",
        iconCls: "fa fa-map-marker start-icon",
        callback: function() {
            onAddStartClick();
        }
    }, {
        text: "Add destination",
        iconCls: "fa fa-map-marker destination-icon",
        callback: function() {
            addDestinationClick()
        }
    }];
}

function onAddStartClick() {
    if (allMarkers[0].getLatLng() != null) {
        addNewStartPoint();
    }
    allMarkers[0].setLatLng(lastClickedPosition).addTo(map);
    map.setView(lastClickedPosition);
    findAddressFromCoords(0, lastClickedPosition);
    getPlansFromServer();
}

function addDestinationClick() {
    if (allMarkers[allMarkers.length-1].getLatLng() != null) {
        addNewDestinationPoint();
    }
    allMarkers[allMarkers.length-1].setLatLng(lastClickedPosition).addTo(map);
    map.setView(lastClickedPosition);
    findAddressFromCoords(allMarkers.length - 1, lastClickedPosition);
    getPlansFromServer();
}

function addSuperchargersLayerFromFile() {
    $.get("./csv/ge-superchargers-with-chtime.csv", function(data) {
        var myIcon = L.divIcon({
            iconSize: [25, 25],
            iconAnchor: [12,25],
            className: "supercharger-marker",
            html: '<img src="./img/tesla-icon.png">'
        });
        var superchargersLayer = L.geoCsv(data, {firstLineTitles: true, fieldSeparator: ';',
            onEachFeature: function (feature, layer) {
                layer.bindPopup("<dl><dt>" + feature.properties.supercharger + "</dt>" +
                    "<dd>Status: "+ feature.properties.status + "</dd>" +
                    "<dd>Stalls: "+ feature.properties.stalls + "</dd></dl>");
                layer.setZIndexOffset(-1000);
                layer.on("contextmenu", function() {
                    lastClickedPosition = layer.getLatLng();
                    map.contextmenu.showAt(layer.getLatLng());
                });
            },
            pointToLayer: function(feature, latlng) {
                return L.marker(latlng, {icon: myIcon});
            }
        });
        var overlayMaps = {};
        overlayMaps['Superchargers'] = superchargersLayer;

        L.control.layers(null, overlayMaps).addTo(map);
    });
}