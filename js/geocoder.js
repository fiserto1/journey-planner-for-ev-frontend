//reverse
//https://api.mapbox.com/geocoding/v5/mapbox.places/14.327770399999999%2C50.0422764.json?access_token=?&types=region%2Ccountry

//autocomplete
//https://api.mapbox.com/geocoding/v5/mapbox.places/Berlin%20station%20garten.json?access_token=?&country=de&types=poi&autocomplete=true

var accessToken;

/**
 * initialize geocoder
 */
function initGeocoder(){
    accessToken = 'pk.eyJ1IjoiZmlzZXJ0bzEiLCJhIjoiNmE1NzkzMjQ5ZjdhYTMxZDllNzhlNmQxNGMzZGIyMTAifQ.2xblvAvcBqHdhd3GnKNrbQ';

}



/**
 * set autocomplete to elements with class "whisper"
 */
function setAC() {

    $(".whisper").autocomplete( {
        source: requestAutocompleteAPI,
        minLength: 2,
        select: afterACItemSelect
    });
}

var requestAutocompleteAPI = function( request, response ) {
    console.log(request.term);
    var text = request.term;
    var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + text + '.json?'
        + 'access_token=' + accessToken + "&country=de&autocomplete=true";
    $.ajax( {
        url: url,
        dataType: "json",
        success: function( data ) {
            console.log(data);
            var items = $.map(data.features, function(feature) {
                return {
                    value: feature.place_name,
                    data: feature.geometry.coordinates,
                    id: "tada"
                }
            });
            response(items);
        }
    } );
};

var afterACItemSelect = function(event, ui ) {
    console.log( "Selected: " + ui.item.value + " aka " + ui.item.data );
    /*
     !!DULEZITE!!
     odpoved ze serveru ma prohozene LAT,LNG[leaflet] na LNG,LAT[odpoved]
     */
    var markerIndex = $(this).parent().index();
    var addressLatLon = L.latLng(ui.item.data[1],ui.item.data[0]);
    map.setView(addressLatLon);
    allMarkers[markerIndex].setLatLng(addressLatLon).addTo(map);
    getPlansFromServer();
};


/**
 * reverse geocoding, set address value to input from coordinates
 * @param inputIndex input index in search group
 * @param latLon coordinates
 */
function findAddressFromCoords(inputIndex, latLon) {
    var url = "https://api.mapbox.com/geocoding/v5/mapbox.places/" + latLon.lng + "%2C" + latLon.lat + ".json?" +
        "access_token=" + accessToken + "&types=address";

    $.ajax({
        url: url,
        success: function (data) {
            if (!data.features.length == 0) {
                var address = data.features[0].place_name;
                showAddressInInput(inputIndex, address);
            } else {
                showCoordsInInput(inputIndex, latLon);
            }
        },
        error: function () {
            showCoordsInInput(inputIndex, latLon);
        }
    });

    //var geocoder = L.mapbox.geocoder('mapbox.places');
    //geocoder.reverseQuery(latLon, function(errData, data){
    //    var address = data.features[0].place_name;
    //    if (address == null) {
    //        showCoordsInInput(inputIndex, latLon);
    //    } else {
    //        showAddressInInput(inputIndex, address);
    //    }
    //});
}

function showAddressInInput(inputIndex, address) {
    console.log(address);
    $("#search-group").children().eq(inputIndex).find("input").val(address);
}

function showCoordsInInput(inputIndex, latLon) {
    var value;
    if (latLon == null) {
        value = "";
    } else {
        var value = latLon.lat.toFixed(6) + ", " + latLon.lng.toFixed(6);
    }
    $("#search-group").children().eq(inputIndex).find("input").val(value);
}

function closeAC() {
    $( ".whisper" ).autocomplete( "close");
}

//function findCoordsFromAddress(inputIndex, address) {
//    var url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/' + address + '.json?'
//        + 'access_token=' + accessToken + "&country=de&autocomplete=false";
//
//    $.ajax( {
//        url: url,
//        dataType: "json",
//        success: function( data ) {
//            console.log(data);
//            if (data.features.length != 0) {
//                var addressLatLon = data.features[0].geometry.coordinates;
//                var realAddress = data.features[0].place_name;
//            } else {
//                $("#error-panel").text("Wrong input!").show();
//            }
//        }
//    } );
//}