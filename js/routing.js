var CHEAPEST_JOURNEY = "CHEAPEST_JOURNEY";
var FASTEST_JOURNEY = "FASTEST_JOURNEY";
var MO_FASTEST_JOURNEY = "MO_FASTEST_JOURNEY";

var ROUTE_COLOR = "#aaaaaa";
var BORDER_ROUTE_COLOR = "#888888";

var CHEAPEST_ROUTE_COLOR = '#99CC00';
var FASTEST_ROUTE_COLOR = '#FF4444';
var MO_FASTEST_ROUTE_COLOR = '#1c94c4';

var CHEAPEST_ROUTE_BORDER_COLOR = "#70A300";
var FASTEST_ROUTE_BORDER_COLOR = "#D30808";
var MO_FASTEST_ROUTE_BORDER_COLOR = "#165694";
//#CC0000
var ROUTE_OPACITY = 1;
var BORDER_ROUTE_OPACITY = 1;
var HIGHLIGHT_ROUTE_OPACITY = 1;

var ROUTE_WEIGHT = 5;
var BORDER_ROUTE_WEIGHT = 7;
var HIGHLIGHT_ROUTE_WEIGHT = 5;

var chartOptions;

var BASIC_ROUTE_OPTIONS = {
    color: ROUTE_COLOR,
    weight: ROUTE_WEIGHT,
    opacity: ROUTE_OPACITY,
    lineJoin: 'round',
    lineCap: 'round',
    smoothFactor: 0
};

var BORDER_ROUTE_OPTIONS = {
    color: BORDER_ROUTE_COLOR,
    weight: BORDER_ROUTE_WEIGHT,
    opacity: BORDER_ROUTE_OPACITY,
    lineJoin: 'round',
    lineCap: 'round',
    smoothFactor: 0
};

var HIGHLIGHT_ROUTE_OPTIONS = {
    color: ROUTE_COLOR,
    weight: HIGHLIGHT_ROUTE_WEIGHT,
    opacity: HIGHLIGHT_ROUTE_OPACITY,
    lineJoin: 'round',
    lineCap: 'round',
    smoothFactor: 0
};


var basicRoutes = L.layerGroup();
var basicRouteBorders = L.layerGroup();
var response;
var highlightRoute = null;
var highlightRouteWithBorder = L.layerGroup();

/**
 * recognize hash and load plans from responseId
 */
function initializeRouting() {
    var hash = location.hash;
    if (hash != "") {
        hash = hash.substr(1).split("&");
        showPlanFromHash(hash);
    }

    $("#cancel-button").click(cancelPlans);
}

function showPlanFromHash(hash) {
    if (hash[0] == "demo") {
        showCoordsInInput(0, demoStartLatLng);
        showCoordsInInput(allMarkers.length-1, demoDestLatLng);
        allMarkers[0].setLatLng(demoStartLatLng).addTo(map);
        allMarkers[allMarkers.length-1].setLatLng(demoDestLatLng).addTo(map);
        sliderValue = demoSoc;
        map.setZoom(demoZoom);
        $("#slider").slider("value", demoSoc);
        showSliderValues();
        showPlans(demoResponse);
    } else {

        var startLatLng = L.latLng(hash[0], hash[1]);
        var destinationLatLng = L.latLng(hash[2], hash[3]);
        showCoordsInInput(0, startLatLng);
        showCoordsInInput(allMarkers.length-1, destinationLatLng);
        allMarkers[0].setLatLng(startLatLng).addTo(map);
        allMarkers[allMarkers.length-1].setLatLng(destinationLatLng).addTo(map);
        sliderValue = hash[4];
        moneySliderValue = hash[5];
        //map.setZoom(hash[6]);
        $("#soc-slider").slider("value", hash[4]);
        $("#money-slider").slider("value", hash[5]);
        showSliderValues();
        getPlansFromServer();
    }

}

function cancelPlans() {
    hidePanelsExceptSearch();
    removeAllRoutesFromMap();
    $("#cancel-button").hide();
    location.replace("#");
    for (var i = 0; i < allMarkers.length; i++) {
        map.removeLayer(allMarkers[i]);
        allMarkers[i].setLatLng(null);
        $("#search-panel").find("input").val("");
    }
}

function hidePanelsExceptSearch() {
    $("#routes-panel").html("").hide();
    $("#error-panel").hide();
    $("#legend").hide();
}

function removeAllRoutesFromMap() {
    map.removeLayer(basicRoutes);
    map.removeLayer(basicRouteBorders);
    map.removeLayer(highlightRouteWithBorder);
    for (var i = 0; i < superchargerMarkers.length; i++) {
        map.removeLayer(superchargerMarkers[i]);
        superchargerMarkers[i].clearLayers();
    }
    basicRoutes.clearLayers();
    basicRouteBorders.clearLayers();
    highlightRouteWithBorder.clearLayers();
}



/**
 * post json object with start and destination coordinates, then get response by responseId
 */
function getPlansFromServer() {
    removeAllRoutesFromMap();
    hidePanelsExceptSearch();
    if (allMarkers.length < 2) {
        return;
    }
    var startMarker = allMarkers[0];
    var destinationMarker = allMarkers[allMarkers.length-1];
    if (destinationMarker.getLatLng() != null && startMarker.getLatLng() != null) {
        var target = document.getElementById('map');
        //$('#wait-screen').show(); //to disable page while loading
        spinner.spin(target);
        var data = createRequestData(startMarker, destinationMarker);
        console.log("sending request:")
        console.log(data);
        console.log(JSON.stringify(data));

        $.ajax({
            method: "POST",
            url: "https://db1.umotional.net/charge-here/v1/journeys?routerTypes=MO_FAST",
            //url: "http://localhost:8080/v1/journeys?routerTypes=MO_FAST", //&routerTypes=FAST",
            contentType: "application/json",
            //dataType: 'json',
            //crossDomain: true,
            //headers: {"Content-Type": "application/json"},
            data: JSON.stringify(data),
            error: function(xhr,status,error) {
                var errorCode = xhr.status;
                handleServerError(errorCode);
            },
            success: function(data, status, xhr) {
                console.log('response:');
                console.log(data);
                //console.log(status);
                //console.log(xhr);
                //console.log(xhr.getAllResponseHeaders());
                if (data.status == 0) {
                    var hash = "#" + (startMarker.getLatLng().lat).toFixed(6) +"&"+ (startMarker.getLatLng().lng).toFixed(6)+
                        "&" + (destinationMarker.getLatLng().lat).toFixed(6) +"&"+ (destinationMarker.getLatLng().lng).toFixed(6) +
                        "&" + sliderValue + "&" + moneySliderValue + "&" + map.getZoom();
                    location.replace(hash);
                    showPlans(data);
                } else {
                    handleServerError(502)
                }
            }
        });
    }
}

function createRequestData(startMarker, destinationMarker) {
    return {
        "client":"journey-planner-for-EV",
        "origin":{
            //"type":"ORIGIN",
            "latE6": (startMarker.getLatLng().lat * 1000000).toFixed(0),
            "lonE6": (startMarker.getLatLng().lng * 1000000).toFixed(0)
        },
        "destination":{
            //"type":"DESTINATION",
            "latE6": (destinationMarker.getLatLng().lat * 1000000).toFixed(0),
            "lonE6": (destinationMarker.getLatLng().lng * 1000000).toFixed(0)
        },
        "stateOfChargeInPerc": sliderValue,
        "centsPerHour": moneySliderValue*100
    };
}

function handleServerError(errorCode) {
    spinner.stop();
    //$('#wait-screen').hide(); //to disable page while loading
    if (errorCode == 400) {
        //$("#error-panel").text("Out of range!").show();
        $("#error-panel").text("Origin or Destination is outside Germany!").show();
    } else if (errorCode == 404 ) {
        //$("#error-panel").text("Your car range is too small!").show();
        $("#error-panel").text("Initial battery state of charge is too low!").show();
    } else if (errorCode >= 500 && errorCode < 510) {
        //$("#error-panel").text("Your car range is too small!").show();
        $("#error-panel").text("Something is wrong!").show();
    } else {
        $("#error-panel").text("Server is down!").show();
    }
}

/**
 * show plans from response json object to the map
 * @param obj
 */
function showPlans(obj) {
    response = obj;

    $("#cancel-button").show();
    //if (obj.status == "OUT_OF_BOUNDS") {
    //    handleServerError(400);
    //    return;
    //}

    spinner.stop();
    //$('#wait-screen').hide(); //to disable page while loading

    var plans = response.plans;

    //if (plans.length > 8)

    iterateAllPlans(plans);
    $(".route-but").on('click', routeButtonClick);
    //$(".length-button").on('click', disableClick);

    basicRouteBorders.addTo(map);
    basicRoutes.addTo(map);
    $("#routes-panel").show("blind");
    $('[data-toggle="tooltip"]').tooltip();
    $("#legend").show();

    //setTimeout(function(){
    //    for (var i = 0; i < plans.length; i++) {
    //        createSOCChart(i);
    //    }
    //
    //}, 20);// does not work without timeout
    highliqhtClickedRoute(0);
    //for (var i = 0; i < plans.length; i++) {
    //    updateSegmentLengthRatio(plans[i], i); //updates green segments in plan description
    //}
}


function createLatLng(coordinates) {
    var lat = coordinates.latE6 / 1E6;
    var lng = coordinates.lonE6 / 1E6;
    return L.latLng(lat, lng);
}
/**
 * save step coordinates to polylines
 * @param plans
 */
function iterateAllPlans(plans) {
    allChartOptions.data = [];
    allChartOptions.usedSteps = [];
    for (var i = 0; i < plans.length; i++) {
        var oneBasicRouteLatLngs = [];
        //var steps = plans[i].steps;
        var segments = plans[i].segments;
        var statesOfCharge = [];
        var lengths = [];
        var travelTimes = [];
        var XYData = [];
        var usedSteps = [];
        var chargers = [];
        var travelTime = 0;
        var length = 0;
        var numOfSOC = 0;

        for (var j = 0; j < (segments.length); j++) {
            var from = segments[j].from;
            if (from.type == "SUPERCHARGER") {
                chargers.push(from);
            }


            var steps = [];
            steps = steps.concat(from, segments[j].viaSteps);
            //console.log(steps);

            var to = segments[j].to;
            if (to.type == "DESTINATION") {
                steps.push(to);
            }

            for (var k = 1; k < (steps.length-1); k++) {
                var latLon = createLatLng(steps[k].coordinates);
                oneBasicRouteLatLngs.push(latLon);
                var stateOfCharge = steps[k].leavingStateOfCharge;
                var maxSoc = 85000;
                if (stateOfCharge > 0) {
                    //console.log(length+","+stateOfCharge + ", " + L.latLng(lat, lng));
                    stateOfCharge = (stateOfCharge/maxSoc)*100;
                    statesOfCharge.push(stateOfCharge);
                    //console.log(stateOfCharge + ", " + length);
                    lengths.push(length);
                    travelTimes.push(travelTime);
                    XYData.push([length, stateOfCharge]);
                    usedSteps.push(latLon);
                    numOfSOC++;
                }

                var ttToNext = steps[k].travelTimeToNextStep;
                var distToNext = steps[k].distanceToNextStep;
                if (distToNext > 0) {
                    length += distToNext;
                    travelTime += ttToNext;
                }
            }



        }
        //for (var j = 1; j < (steps.length-1); j++) {
        //    var coordinate = steps[j].coordinate;
        //    var lat = coordinate.latE6 / 1E6;
        //    var lng = coordinate.lonE6 / 1E6;
        //    oneBasicRouteLatLngs.push(L.latLng(lat, lng));
        //
        //    var stateOfCharge = steps[j].leavingStateOfCharge;
        //    var maxSoc = 85000;
        //    if (stateOfCharge > 0) {
        //        //console.log(length+","+stateOfCharge + ", " + L.latLng(lat, lng));
        //        stateOfCharge = (stateOfCharge/maxSoc)*100;
        //        statesOfCharge.push(stateOfCharge);
        //        //console.log(stateOfCharge + ", " + length);
        //        lengths.push(length);
        //        XYData.push([length, stateOfCharge]);
        //        usedSteps.push(L.latLng(lat, lng));
        //        numOfSOC++;
        //    }
        //
        //    var distToNext = steps[j].distanceToNextStep;
        //    if (distToNext > 0) {
        //        length += distToNext;
        //    }
        //}

        addPlanSuperchargerMarkers(chargers, i);
        var oneBasicRoute = L.polyline(oneBasicRouteLatLngs, BASIC_ROUTE_OPTIONS);
        var oneBasicRouteBorder = L.polyline(oneBasicRouteLatLngs, BORDER_ROUTE_OPTIONS);
        oneBasicRoute.on('click', routeClick);

        //if (plans[i].type == FASTEST_JOURNEY) {
        //    oneBasicRoute.setStyle({color: FASTEST_ROUTE_COLOR});
        //    oneBasicRouteBorder.setStyle({color: FASTEST_ROUTE_BORDER_COLOR});
        //} else if (plans[i].type == CHEAPEST_JOURNEY) {
        //    oneBasicRoute.setStyle({color: CHEAPEST_ROUTE_COLOR});
        //    oneBasicRouteBorder.setStyle({color: CHEAPEST_ROUTE_BORDER_COLOR});
        //} else if (plans[i].type == MO_FASTEST_JOURNEY) {
        //    oneBasicRoute.setStyle({color: MO_FASTEST_ROUTE_COLOR});
        //    oneBasicRouteBorder.setStyle({color: MO_FASTEST_ROUTE_BORDER_COLOR});
        //}

        //var criteria = new Criteria(plans[i].travelTime, plans[i].length, plans[i].consumption, plans[i].price);
        //journeys.push(new Journey(plans[i].type, oneBasicRoute, oneBasicRouteBorder, criteria));

        basicRoutes.addLayer(oneBasicRoute);
        basicRouteBorders.addLayer(oneBasicRouteBorder);
        createButtonForRoute(plans[i], i);
        allChartOptions.data.push(XYData);
        allChartOptions.usedSteps.push(usedSteps);
        allChartOptions.travelTimes.push(travelTimes);
        //console.log(statesOfCharge);
    }

}

/**
 * action listener for click on the route on the map
 * @param e
 */
function routeClick(e) {
    var routeIndex = basicRoutes.getLayers().indexOf(e.target);
    highliqhtClickedRoute(routeIndex);
}

function disableClick(e) {
    var routeIndex = $('.length-button').index(e.currentTarget);
    highliqhtClickedRoute(routeIndex);
}

function routeButtonClick(e) {
    var routeIndex = $('.route-but').index(e.currentTarget);
    highliqhtClickedRoute(routeIndex);
}


function highliqhtClickedRoute(routeIndex) {
    //$(".color-tab").removeClass("clicked-but");
    //$(".route-desc").eq(routeIndex).find(".color-tab").addClass("clicked-but");
    $(".main-criteria").removeClass("clicked-but");
    $(".route-desc").eq(routeIndex).find(".main-criteria").addClass("clicked-but");


    //$(".plan-view").hide();
    //$(".route-desc").eq(routeIndex).find(".plan-view").show("blind");
    //$("#route-but-" + routeIndex).find(".route-desc").addClass("clicked-but");
    var clickedRoute = basicRoutes.getLayers()[routeIndex];
    map.removeLayer(highlightRouteWithBorder);
    highlightRouteWithBorder.clearLayers();

    var hRoute = L.polyline(clickedRoute.getLatLngs(), HIGHLIGHT_ROUTE_OPTIONS);
    var hRouteBorder = L.polyline(clickedRoute.getLatLngs(), BORDER_ROUTE_OPTIONS);

    var type = response.plans[routeIndex].type;
    if (type == CHEAPEST_JOURNEY) {
        hRoute.setStyle({color: CHEAPEST_ROUTE_COLOR});
        hRouteBorder.setStyle({color: CHEAPEST_ROUTE_BORDER_COLOR});
    } else if (type == FASTEST_JOURNEY) {
        hRoute.setStyle({color: FASTEST_ROUTE_COLOR});
        hRouteBorder.setStyle({color: FASTEST_ROUTE_BORDER_COLOR});
    } else if (type == MO_FASTEST_JOURNEY) {
        hRoute.setStyle({color: MO_FASTEST_ROUTE_COLOR});
        hRouteBorder.setStyle({color: MO_FASTEST_ROUTE_BORDER_COLOR});

    } else {
        error.log("Unrecognized type of journey.");
    }

    for (var j=0; j < response.plans.length; j++) {
        if (j == routeIndex) continue;
        $(".route-desc").eq(j).find(".plan-view").hide("blind");
    }
    $(".route-desc").eq(routeIndex).find(".plan-view").show("blind");

    highlightRouteWithBorder.addLayer(hRouteBorder);
    highlightRouteWithBorder.addLayer(hRoute);
    highlightRouteWithBorder.addTo(map);

    for (var i=0; i<superchargerMarkers.length; i++) {
        map.removeLayer(superchargerMarkers[i]);
    }
    superchargerMarkers[routeIndex].addTo(map);
    createSOCChart(routeIndex);

    //$("#route-but-0").tab("hide");
    //for (var i=0; i<superchargerMarkers[routeIndex].length; i++){
    //    superchargerMarkers[routeIndex][i].setZIndexOffset(1000000);
    //
    //}
}