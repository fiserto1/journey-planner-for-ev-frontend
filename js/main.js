/**
 * run web application after document ready
 * show smartbanner for mobile devices
 * main -> spinner -> translation -> sidebar -> map -> markers -> segments -> routing
 */
$(document).ready(function() {

    initializeSpinner();
    initializeSlider();
    initGeocoder();

    initializeMap();
    initializeMarkers();
    initializeRouting();
    $('[data-toggle="tooltip"]').tooltip();
    setAC();
});