var sliderValue;
var moneySliderValue;

function initializeSlider() {


    initSocSlider();

    initMoneySlider();

    showSliderValues();
}

function showSliderValues() {
    $('#soc-slider-value').text("SoC: " + sliderValue + "%");
    $('#cph-slider-value').text("CpH: " + moneySliderValue + "€");
}

function initSocSlider() {
    sliderValue = 50;

    $("#soc-slider").slider({
        range: "min",
        min: 0,
        max: 100,
        step: 5,
        value: sliderValue,
        slide: function( event, ui ) {
            sliderValue = ui.value;
            showSliderValues();
        },
        stop: function(event, ui) {
            getPlansFromServer();
        }
    });
}

function initMoneySlider() {
    moneySliderValue = 0.4;

    $("#money-slider").slider({
        range: "min",
        min: 0,
        max: 5,
        step: 0.2,
        value: moneySliderValue,
        slide: function( event, ui ) {
            moneySliderValue = ui.value;
            showSliderValues();
        },
        stop: function(event, ui) {
            getPlansFromServer();
        }
    });
}