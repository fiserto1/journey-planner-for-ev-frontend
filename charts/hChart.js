var allChartOptions = {
    max:110,
    min:0,
    usedSteps: [],
    travelTimes: [],
    data: [] //0-cheapest, 1-fastest
};
var circle;
var segmentLine;
var segmentLineBorder;
var segmentLineShadow;

/*
 colored zones in charts
 */
//var SPEED_ZONES = [{
//    value: SPEED_LIMIT_LVL_1,
//    color: SPEED_COLOR_LVL_1
//}, {
//    value: SPEED_LIMIT_LVL_2,
//    color: SPEED_COLOR_LVL_2
//}, {
//    value: SPEED_LIMIT_LVL_3,
//    color: SPEED_COLOR_LVL_3
//}, {
//    value: SPEED_LIMIT_LVL_4,
//    color: SPEED_COLOR_LVL_4
//}, {
//    color: SPEED_COLOR_LVL_5
//}];


/*
data format: [[x1,y1],[x2,y2],[x3,y3],...]
dataType = speed/stress/power
var options = {
    data: [],
    routeIndex: 1,
    target: divElement,
    max: maxValue,
    min: minValue,
    dataType: (speed/stress/power),
    pointerColor: "#ff0000"
};
*/

var hChart
/**
 * Creates chart from options
 * var options = {
 *    data: [[x1,y1],[x2,y2],[x3,y3],...],
 *    routeIndex: 1,
 *    target: divElement,
 *    max: maxValue,
 *    min: minValue,
 *    dataType: (speed/stress/power),
 *    pointerColor: "#ff0000"
 *  };
 * @param options
 */
function createChart(options) {
    hChart = options.target.highcharts({
        chart: {
            zoomType: 'xy',
            borderRadius: 0,
            marginTop: 30,
            marginRight: 25,
            marginLeft: 52,
            marginBottom: 25
        },
        title: {
            text: "State of charge profile"
        },
        xAxis: {
            tickWidth: 0,
            maxPadding: 0,
            minPadding: 0,
            allowDecimals: true,
            gridLineWidth: 0,
            lineWidth: 0,
            labels: {
                enabled: true,
                formatter: function () {
                    return this.value/1000 + ' km';
                }
            }
        },
        yAxis: {
            showFirstLabel: true,
            showLastLabel: true,
            tickPositions: [0, 100],
            title: {
                text: null
            },
            tickWidth: 0,
            gridLineWidth: 0,
            lineWidth: 0,
            endOnTick: false,
            startOnTick: false,
            max: options.max,
            min: options.min,
            labels: {
                enabled: true,
                formatter: function () {
                    return this.value + ' %';
                }
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            enabled: false,
            crosshairs: [{
                dashStyle: 'dash'
            }, {
                dashStyle: "dash"
            }]
        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                marker: {
                    radius: 2,
                    lineWidth: 1,
                    fillColor: options.pointerColor
                }
            }
        },
        series: [{
            lineWidth: 2,
            fillColor : {
                linearGradient : {
                    x1: 0,
                    y1: 0,
                    x2: 0,
                    y2: 1
                },
                stops : [
                    [0, Highcharts.getOptions().colors[0]],
                    [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                ]
            },
            animation: false,
            data: options.data,
            //zones: options.zones,
            point: {
                events: {
                    mouseOver: function (e) {
                        var chartValueIndex = e.target.index;
                        //var planSteps = response.plans[options.routeIndex].steps;
                        //var coord = planSteps[chartValueIndex].coordinate;
                        //var latLng = L.latLng(coord.latE6/1000000,coord.lonE6/1000000);
                        var latLngCircle = allChartOptions.usedSteps[options.routeIndex][chartValueIndex];
                        var ttFromStart = allChartOptions.travelTimes[options.routeIndex][chartValueIndex];
                        //var nextCoord = planSteps[chartValueIndex + 1].coordinate;
                        //var nextLatLng = L.latLng(nextCoord.latE6/1000000,nextCoord.lonE6/1000000);
                        var segmentLineOptions;
                        var segmentLineBorderOptions;
                        var segmentLineShadowOptions;
                        var popupOptions = {autoPan: false, closeButton: false};
                        var popupString = "";

                        segmentLineOptions = {
                            color: "#FF0000",
                            weight: 7,
                            opacity: 1
                        };
                        segmentLineBorderOptions = {
                            color: "white",
                            weight: 10,
                            opacity: 1
                        };
                        segmentLineShadowOptions = {
                            color: "#FF0000",
                            weight: 17,
                            opacity: 0.7
                        };
                        popupString = "<dl><dd>State Of Charge: " + this.y.toFixed(1) + " %</dd>" +
                            "<dd> From Start: " + (this.x/1000).toFixed(0) +" km</dd>" +
                            "<dd> From Start: " + (ttFromStart/60000).toFixed(0) +" min</dd></dl>";

                        circle = L.circleMarker(latLngCircle, {color: 'dodgerblue'});
                        circle.setRadius(7);
                        circle.bindPopup(popupString, popupOptions);
                        circle.addTo(map);
                        circle.openPopup();
                        //segmentLine = L.polyline([latLng,nextLatLng], segmentLineOptions);
                        //segmentLineBorder = L.polyline([latLng,nextLatLng], segmentLineBorderOptions);
                        //segmentLineShadow = L.polyline([latLng,nextLatLng], segmentLineShadowOptions);
                        //
                        //segmentLineShadow.addTo(map);
                        //segmentLineBorder.addTo(map);
                        //segmentLine.addTo(map);
                        //segmentLine.bindPopup(popupString, popupOptions).openPopup();
                    },
                    mouseOut: function () {
                        map.removeLayer(circle);
                        //map.removeLayer(segmentLine);
                        //map.removeLayer(segmentLineBorder);
                        //map.removeLayer(segmentLineShadow);
                    }
                }
            }
        }]
    });
}



function createSOCChart(routeIndex) {
    var hChart = null;
    var chartDivId = "#soc-chart-" + routeIndex;
    //console.log(allChartOptions.data[routeIndex]);
    var options = {
        target: $(chartDivId),
        data: allChartOptions.data[routeIndex],
        max: allChartOptions.max,
        min: allChartOptions.min,
        routeIndex: routeIndex,
        dataType: 1,
        pointerColor: 'dodgerblue'
        //zones: SPEED_ZONES
    };
    createChart(options);
}
